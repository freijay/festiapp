
/*
 * 
 * FestiApp 1.0 - Client-side sorting elements with dynamic filters building.
 * @requires jQuery
 * 
 * Copyright (c) 2017 Jérôme Freiburghuas
 * license: MIT
 *
 * Examples and docs at: https://bitbucket.org/freijay/festiapp
 * 
 */

/* jshint undef: false, unused: false, loopfunc: true */
/* globals open:true */
/* jshint esnext: true */

var festiApp = festiApp || { 		   
    options : {					
		json : 'https://spreadsheets.google.com/feeds/list/1eMCIUrkpEOa-pIFCy63cW_XmoH2tyPwx94XO0VDiMAA/od6/public/values?alt=json',		
		lang : {
			fr : "fr",
			de : "de"							
		},
		localisation : {
			day : {
				d102 : 'Monday',					
				d103 : 'Tuesday',
				d104 : 'Wednesday',
				d105 : {
					fr : 'Jeudi', 
					de : 'Donnerstag' 															
				},
				d106 : {
					fr : 'Vendredi', 
					de : 'Freitag'					
				},				
				d107 : {
					fr : 'Samedi', 
					de : 'Samstag' 

				},
				d108 : {
					fr : 'Dimanche', 
					de : 'Sonntag' 															
				}				
			},				
			scene : {
				sc001: 'Le Chapiteau',
				sc002: 'La Lacustre',
				sc003: 'La Marée',
				sc004: 'Art de rue',
				sc005: 'Case à Chocs',
				sc006: 'Le Phare',
				sc007: 'La Plage'							
			},				
			style : {				
				st101:'fusion rap metal',
				st102:'électro',
				st103:'funk',
				st104:'punk',
				st105:'rock',
				st106:'hip-hop',
				st107:'rap',
				st108:'Gardes du corps encombrants',
				st109:'mignon & sanglant',
				st110:'chanson française',
				st112:'dance',
				st113:'pop',
				st114:'folk',
				st116:'chicha',
				st117:'bass',
				st118:'house',
				st119:'afro',
				st121:'reggae',
				st122:'new roots',
				st123:'dancehall',
				st124:'jazz',
				st125:'techno',
				st127:'kuduro',
				st128:'jazz moderne',
				st129:'jouet vivant',
				st130:'zouglo',
				st132:'balkan',
				st133:'klezmer',
				st134:'orchestre',
				st135:'ska',
				st136:'jeune public',
				st137:'rap acoustique',
				st138:'disco'																																											
			}			
		}
			
	},
	_:	function(options){
		this.options = jQuery_3_2_1.extend(this.options, options);	
				
		var path = location.pathname;

		lang = "fr";

		if(path.indexOf("/de/") !== -1){
			lang = "de";								
		}

		// this.utility.lang();									
		this.load();
	},
	load : function(){
		jQuery_3_2_1.ajax({
			url: this.options.json,
			crossDomain: true,
			type: 'GET',
			cache : false,			    
			contentType: "application/json",
			dataType: 'jsonp',
			success: function(json) {							
				festiApp.concert.build(json);				
		    },
			error: function() {					
				console.log('Error - concert loading');
			},
			complete: function() {		
				console.log('Success - concert loading');			
			},	
		}); 		
	},
	utility : {		
		lang : function(){			
			lang = festiApp.options.lang;			
			var html = 	"<ul class='lang'>";
			jQuery_3_2_1.each(lang,function(key, val){				
				if(key === "default"){					
					key = key + " active";										
				}
				html += '<li><a class="'+key+'">'+val+'</a></li>';											
			});
			html += "</ul>";		
			jQuery_3_2_1(html).appendTo("div.filters");
			
			jQuery_3_2_1("ul.lang li a").click(function(){				
				var lang = jQuery_3_2_1(this).attr("class"),
					selected = "data-lang-"+lang;				
				jQuery_3_2_1("ul.lang li a").removeClass("active");				
				jQuery_3_2_1(this).addClass("active");								
				filter = jQuery_3_2_1("a.filter");				
				filter.each(function(){				
					if(jQuery_3_2_1(this).attr(selected)){						
						var txt = jQuery_3_2_1(this).attr(selected);						
						jQuery_3_2_1(this).text(txt);																		
					}									
				});				
				ga('send', 'event', 'click', 'language' , lang );											
			});																					
		},		
		sortConcert : function($elem, $items){									
			$items.sort(function(a, b) {
			  var nameA = a.innerHTML.toUpperCase(); 
			  var nameB = b.innerHTML.toUpperCase();			  
			  if (nameA < nameB) {							  
			    return -1;
			  }
			  if (nameA > nameB) {
			    return 1;
			  }
			  return 0;			  
			});									
			jQuery_3_2_1($elem).append($items);						
		},
		sortFilter : function($ul){									
			var $li = $ul.children("li");
			$li.sort(function(a, b) {
			  var nameA = jQuery_3_2_1(a).children().text().toUpperCase(); 
			  var nameB = jQuery_3_2_1(b).children().text().toUpperCase();			  
			  if (nameA < nameB) {							  
			    return -1;
			  }
			  if (nameA > nameB) {
			    return 1;
			  }
			  return 0;			  
			});									
			jQuery_3_2_1($ul).append($li);						
		}						
	},
	controler : {			
		init : function(){		
			$filter = jQuery_3_2_1("a.filter");
			$concert = jQuery_3_2_1("div.concerts");						
			$filter.click(function(){			
				if(!jQuery_3_2_1(this).hasClass("deactivate")){				
					active = [];
					$filter.removeClass("deactivate");					
					jQuery_3_2_1(this).toggleClass("active");										
					for (var i = 0; i < $filter.length; i++){ 												
						x = jQuery_3_2_1($filter[i]);						
						if(x.hasClass("active")){					
							active.push(x.attr("data-filter"));					
						}																
					}					
					display = [[],[]];        				

					for (var f = 0; f < $concert.length; f++){ 																	
						var item = jQuery_3_2_1($concert[f]),				
							item_filters = item.attr('data-filters'),
							item_show = true;													
						for (var y = 0; y < active.length; y++){ 																	
							if(item_filters.indexOf(""+active[y]+"") === -1){
								item_show = false;									
								if(display[0].indexOf(item) === -1){						
									display[0].push(item);						
								}
							}	             	              	              	              	              	             	            	              					
						}
			            if(item_show === true) {
							display[1].push(item);							
			            }														
					}
			        if(display[0].length > 0){
			          	festiApp.controler.hide(display[0]);
					  	jQuery_3_2_1("div.reset").show();				        				        			          
			        }
			        if(display[1].length > 0){
			          festiApp.controler.show(display[1]);        
			        } 												
			       
					if(active.length === 0){
						jQuery_3_2_1("div.reset").hide();				        				        			          						
					}
			       
					var filter = jQuery_3_2_1(this).attr("data-lang-default");
					var cat = jQuery_3_2_1(this).parents("ul").attr("class").split("-")[1];
																						    			    
					ga('send', 'event', 'filter', cat , filter );
			    			    			       
				}							
			});
			jQuery_3_2_1("div.reset").click(function(){								
				$filter.removeClass("deactivate active");
				$concert.show(); 				
				jQuery_3_2_1("div.reset").hide();
				ga('send', 'event', 'filter', 'button' , 'Reset' );								
			});			
			jQuery_3_2_1(".expand").click(function(event){
				var article = jQuery_3_2_1(this).children("article");
				
				article.toggle();
												
				if(article.is(":visible")){
					var title = jQuery_3_2_1(this).find(".title").text();
					ga('send', 'event', 'click', 'concert' , title );
																												
				}
			});					
		},
	    show: function(array){
			for (var i = 0; i < array.length; i++){		  
	        	if(!array[i].is(':visible')){
				array[i].fadeIn(200);			
	        }        		  
			festiApp.filter.status(array);                           
		  }  	            
	    },
	    hide: function(array){
			for (var i = 0; i < array.length; i++){		  
	        	if(array[i].is(':visible')){
					array[i].fadeOut(200);
	        	}
			}
	    }										
	},
	filter : {				
		status : function(array){			
			var show = [];
			for (var i = 0; i < array.length; i++) {	 										
				var y = jQuery_3_2_1(array[i]).attr("data-filters"),	
					x = y.split(",");														
				for (var a = 0; a < x.length; a++) {						
					if(show.indexOf(x[a]) === -1){						
						show.push(x[a]);
					}
				}												
			}
			jQuery_3_2_1($filter).addClass("deactivate");																		
			for (var c = 0; c < $filter.length; c++) {
				var b = jQuery_3_2_1($filter[c]).attr("data-filter");				
				for (var d = 0; d < show.length; d++){	
					if( show[d] === b){						
						jQuery_3_2_1($filter[c]).removeClass("deactivate");						
					}										
				}														
			}												
		},
		build : function(array){
			var filter = [];
			for (var i = 0; i < array.length; i++) {	 										
				var y = jQuery_3_2_1(array[i]).attr("data-filters"),				
					x = y.split(",");														
				for (var e = 0; e < x.length; e++) {						
					if(filter.indexOf(x[e]) === -1){						
						filter.push(x[e]);
					}
				}												
			}
			this.output(filter);
		},
		output : function(filter){				
			localisation = festiApp.options.localisation;						
			jQuery_3_2_1.each(localisation,function(key, val){				
				var html = 	"<ul class='filters filter-"+key+"'>";
				for (var i = 0; i < filter.length; i++) {	 																
				    jQuery_3_2_1.each(val, function(key, val) {																																													
						if(key === filter[i]){
							
							datalang = "";
							
							if(typeof val === "object"){								

								datalang = [];

								jQuery_3_2_1.each(val, function(key, val) { 	
									var data = 'data-lang-'+key+'="'+val+'"';																		
									datalang.push(data);																												
								});	
																								
								datalang = datalang.join(" ");
								val = val[lang];																																	
							}								
							html += '<li><a class="filter" data-lang-default="'+val+'" '+datalang+' data-filter="'+filter[i].replace(/\s/g, '').toLowerCase()+'">'+val+' <div class="cross">x</div></a></li>';											
						}											
				    });						    				
				}
				html += "</ul>";		
				jQuery_3_2_1(html).appendTo("div.filters");											
			});
			festiApp.utility.sortFilter(jQuery_3_2_1("ul.filter-style"));			
		}						
	},
	concert : {		
		build : function(json){
			var concert = [],
				feed = json.feed.entry;
	        for (var i = 0; i < feed.length; i++) {
				var filters =  feed[i].gsx$day.$t +','+ feed[i].gsx$style.$t +','+ feed[i].gsx$scene.$t,
					desc = "",
					url = "",
					wording_more = "",
					scene = "",
					days="",
					link="",
					expand ="";
					origin = "";
					
				var localisation = festiApp.options.localisation.scene,
					styles = festiApp.options.localisation.style,
					day = festiApp.options.localisation.day;
					
				jQuery_3_2_1.each(localisation,function(key, val){						
					if(feed[i].gsx$scene.$t === key){						
						scene = val;						
					}												
				});	

				var da = [],
					temp2 = feed[i].gsx$day.$t.split(",");
				
				for (var x = 0; x < temp2.length; x++){ 																		
					jQuery_3_2_1.each(day,function(key, val){						
						if(temp2[x] === key){						
							da.push(val[lang]);						
						}												
					});																				
				}				
				var styl = [],
					style = feed[i].gsx$style.$t,
					temp = style.split(",");
				
				for (var y = 0; y < temp.length; y++){ 																		
					jQuery_3_2_1.each(styles,function(key, val){						
						if(temp[y] === key){						
							styl.push(val);						
						}												
					});																				
				}																														
				if(lang === "de"){																				
					if(feed[i].gsx$descriptionde.$t){						
						desc = feed[i].gsx$descriptionde.$t;									
					}											
					if(feed[i].gsx$url.$t){						
						var split = feed[i].gsx$url.$t.split(".ch/");
						url = split[0] + ".ch/de/"+split[1];																																
					}																					
					wording_more = "Weitere Informationen";														
				}
				if(lang === "fr"){					
					desc = feed[i].gsx$descriptionfr.$t;
					url =  feed[i].gsx$url.$t;					
					wording_more = "Plus d'info";						
				}									
				if(feed[i].gsx$url.$t){										
					link = "<a href='"+url+"' class='link'>"+wording_more+"</a>";										
				}
				
				if(desc){					
					expand = "expand";					
				}
				
				if(feed[i].gsx$origin.$t){
					
					origin = "["+feed[i].gsx$origin.$t+"]";
					
				}																																			
				concert.push({
				   "id": feed[i].gsx$id.$t,
				   "name": feed[i].gsx$name.$t,
				   "time": feed[i].gsx$time.$t,				   
				   "filters": filters.replace(/\s/g, '').toLowerCase(),
				   "scene": scene,
				   "style" : styl,
				   "wording_more": wording_more,			   
				   "desc": desc,
				   "url": url,
				   "days" : da,
				   "link" : link,
				   "expand": expand,
				   "origin" : origin
				});					
		    }		    	    		  		    			    		    									
			this.list(concert);		
		},
		output : function(val){		
			var concert = "<div class='concerts "+val.expand+"' id='"+val.id+"' data-filters='"+val.filters+"'><span class='title'>"+val.name+"</span><span class='origin'>"+val.origin+"</span><div><span class='day'>"+val.days+"</span> <span class='time'>"+val.time+"</span> <span class='scene'>"+val.scene+"</span></div><article><div class='style'>"+val.style+"</div>"+val.desc+" <a href='"+val.url+"' "+val.link+"</article><div class='exp'>+</div></div>";
			return concert;											
		},
		list : function(array){						    
		    for (var i = 0; i < array.length; i++) {			    
				jQuery_3_2_1(festiApp.concert.output(array[i])).appendTo("div.concert");			    			    
		    }		 		    		    		    						
			var concert = jQuery_3_2_1("div.concerts");
			
			festiApp.filter.build(concert);		
			festiApp.utility.sortConcert(jQuery_3_2_1("div.concert"),concert);
			festiApp.controler.init();			
		}											
	}	
};

festiApp._();




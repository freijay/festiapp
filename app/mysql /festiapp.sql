SELECT styles.name, GROUP_CONCAT(styles.name)
FROM artistes INNER JOIN artiste_style
ON artistes.id = id_artiste
INNER JOIN styles
ON id_style = styles.id
WHERE artistes.name = "Prophets of Rage";


SELECT scenes.name, concerts.name, artistes.name
FROM concerts INNER JOIN scenes
ON concerts.scene_id = scenes.id
INNER JOIN artistes
on concerts.artiste_id = artistes.id
WHERE concerts.id = "1";


SELECT scenes.name, concerts.name, artistes.name, GROUP_CONCAT(styles.name)
FROM concerts 
INNER JOIN scenes
ON concerts.scene_id = scenes.id
INNER JOIN artistes
ON concerts.artiste_id = artistes.id
INNER JOIN artiste_style
ON artiste_style.id_artiste = concerts.artiste_id
INNER JOIN styles
ON id_style = styles.id
GROUP BY concerts.id


------ LAST -----

SELECT scenes.name AS scene, concerts.name, artistes.name AS artiste, artistes.id AS artiste_id , GROUP_CONCAT(styles.name SEPARATOR " | ") AS style FROM concerts INNER JOIN scenes ON concerts.scene_id = scenes.id INNER JOIN artistes ON concerts.artiste_id = artistes.id INNER JOIN artiste_style ON artiste_style.id_artiste = concerts.artiste_id INNER JOIN styles ON id_style = styles.id GROUP BY concerts.id; SELECT name FROM artistes
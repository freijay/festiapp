app.engine('ntl', function (filePath, options, callback) { // define the template engine
  fs.readFile(filePath, function (err, content) {
    if (err) return callback(err)
    // this is an extremely simple template engine
    
	var size = options.list.length;
		
	var list = '<ul>';
	for (i = 0; i < size; i++) { 		
		list += '<li class="' + options.list[i].artiste + '">' + options.list[i].artiste + ' '+ options.list[i].style +' '+ options.list[i].scene + '</li>';						
	}	
	list += '</ul>';
	
    var rendered = content.toString().replace('#title#', list)
    
    return callback(null, rendered)
  })
})
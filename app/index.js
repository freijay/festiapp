const express = require('express')
const bodyParser = require('body-parser')

const app = express()


app.set('views', './views') // specify the views directory
app.set('view engine', 'pug') // register the template engine

app.use(express.static('public'));

app.get('/', function (req, res) {
    	    
	var mysql      = require('mysql');
	var database = require('./my_modules/access');
	var connection = mysql.createConnection(database);
	 
	connection.connect();	
			 
	connection.query('SELECT artistes.id, artistes.name, GROUP_CONCAT(styles.id,":",styles.name SEPARATOR "|") AS styles FROM artistes LEFT JOIN artiste_style ON artiste_style.id_artiste = artistes.id LEFT JOIN styles ON id_style = styles.id GROUP BY artistes.name', function (error, results, fields) {
	if (error) throw error;
					  	  	  		
		console.log(results)			  	  	  				
		res.render('index', {list: results})
		
	});
 	
	connection.end();        			
})

//app.get('/artistes', function (req, res) {
//    	    
//	var mysql      = require('mysql');
//	var database = require('./my_modules/access');
//	var connection = mysql.createConnection(database);
//	 
//	connection.connect();
//			 
//	connection.query('SELECT name FROM artistes', function (error, results) {
//	if (error) throw error;
//
//
//		
//
//
//		
//		res.render('artistes', options)
//		
//	});
//	connection.end();        			
//})


//support parsing of application/json type post data
app.use(bodyParser.json());

// create application/x-www-form-urlencoded parser 
var urlencodedParser = bodyParser.urlencoded({ extended: false })
 
// POST /login gets urlencoded bodies 
app.post('/', urlencodedParser, function (req, res) {
  if (!req.body) return res.sendStatus(400)
    
	var mysql      = require('mysql');
	var database = require('./my_modules/access');
	var connection = mysql.createConnection(database);
	 
	connection.connect();
			 
	connection.query("UPDATE artistes SET name = '"+req.body.name+"' WHERE id = '1'", function (error, results) {	  
	  if (error) throw error;

	  	console.log(results.affectedRows + ' record(s) updated');
	  	
	  	res.json({status: 200, message : 'success'})
	  	
	});
	
	connection.end();  

})

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})




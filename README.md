# FestiApp v.1.0 #

Customizable jQuery plugins build for sorting elements with dynamic filters building.

### In progress ###

* Add Comments on code

### How it's work ###

Simple client side application, [Google sheet](https://docs.google.com/spreadsheets/d/1eMCIUrkpEOa-pIFCy63cW_XmoH2tyPwx94XO0VDiMAA) used to provide the json file and data management system. Built the concerts listing and provide the filters from the concerts themselves.
Based on Foundation grid

### Demo ###

A demo of FestiApp can be found on [here](http://mylab.fridaymedia.ch/)

### Features ###

* Sorting
* Multi-language
* Customizable 
* Filters categories
* Responsive
* Add google analytics tracking on click even
* Reset button
* Localisation based on URL

###Filters mapping and localization

* Localize only if needed.
* create the categories filters directly on the Object >  DAY - SCENE - STYLE
* Sorting specific filters category by alphabetically.
* Adding data


```
#!javascript
		
// Filters mapping

localisation : {
	day : {
		d105 : {
			fr : 'Jeudi', 
			en : 'Thursday',
			de : 'Donnerstag' 															
		}								
	},				
	scene : {
		sc001: 'Le Chapiteau',
		sc002: 'La Lacustre',			
	},				
	style : {
		st109 : 'mignon & sanglant',
		// Localization example
        st110 : { 
			fr : 'chanson française',
			en : 'french song'					
		}
	}			
}

// Sort "style" filter

festiApp.utility.sortFilter($("ul.filter-style"));

// Adding field - Pull from JSON > Google sheet

concert.push({
   "id": feed[i].gsx$id.$t,
   "url": feed[i].gsx$url.$t,
   "name": feed[i].gsx$name.$t,
   "desc_fr": feed[i].gsx$descriptionfr.$t,
   "desc_de": feed[i].gsx$descriptionde.$t,
   "keywords": feed[i].gsx$keywords.$t,
   "popularity": feed[i].gsx$popularity.$t,
   "filters": filters.replace(/\s/g, '').toLowerCase()
});	

output : function(val){		
	var concert = "<a class='concert' id='"+val.id+"' data-filters='"+val.filters+"' data-link='"+val.url+"' >"+val.name+"<article>"+val.desc_fr+"</article></a>";
	return concert;											
},

```

### Who do I talk to? ###

* Jérôme Freiburghaus - freibj@gmail.com
* Olivier Schwarz - olivier.schwarz@gmail.com